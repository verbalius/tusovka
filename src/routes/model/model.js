﻿var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var artistSchema = Schema({
    _id: Schema.Types.ObjectId, //Artist ID
    name: { type: String, required: true }, //Artist name
    displayDesc: String, //Atrist description (genres)
    username: { type: String, required: true }, //Artist username
    live: {type: Boolean, default: false}, //Is Artist live now
    liveListeners: [{
        user: String,
        startTime: {type: Date, default: new Date()},
        lastUpdate: {type: Date, default: new Date()}
    }], //Array of unique listeners IDs and last update time
    lastStream: Date, //Date of the last stream
    streamUrl: String,
    active: {type: Boolean, default: true} //Artist status
});

mongoose.model('Artist', artistSchema);  