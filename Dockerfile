ARG APP_DIR=/home/node/app
FROM node:17.1.0-alpine
ARG APP_DIR
WORKDIR ${APP_DIR}
RUN chown node:node ${APP_DIR}
COPY --chown=node:node package.json package-lock.json ./
USER node
RUN npm install
COPY --chown=node:node src ./src
EXPOSE 3000
ENTRYPOINT [ "node" ]
CMD [ "src/app.js" ]